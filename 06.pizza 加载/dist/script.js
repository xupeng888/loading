var App = /** @class */ (function () {
    function App() {
        var tl = new TimelineMax({ repeat: -1 });
        tl.to(['.pizzaOutline', '.pizzaMask'], 7, {
            rotation: 360,
            svgOrigin: '61 61',
            ease: Linear.easeNone
        })
            .to('.whole', 7, {
            rotation: -45,
            svgOrigin: '61 61',
            ease: Linear.easeNone
        }, 0);
    }
    return App;
}());
TweenMax.set('svg', {
    visibility: 'visible'
});
var app = new App();
TweenMax.globalTimeScale(4);